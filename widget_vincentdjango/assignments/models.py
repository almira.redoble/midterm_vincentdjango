from django.db import models


class Course(models.Model):
    course_code = models.CharField(unique=True, default="", max_length=10,)
    course_title = models.CharField(unique=True, default="", max_length=50,)
    section = models.CharField(default="",max_length=16,)

    def __str__(self):
        return '{} - {}'.format(self.course_code, self.section,)
    
    def anotherFormat(self):
        return '{} {}'.format(self.course_code, self.course_title,)


class Assignment(models.Model):
    assignment_name = models.CharField("Name", unique=True, default="", max_length=50,)
    description = models.TextField(default="")
    perfect_score = models.IntegerField(default=100)
    passing_score = models.IntegerField(default=80)
    course = models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
        related_name='subject'
    )

    def __str__(self):
        return '{} {}-'.format(self.assignment_name, self.course.section,)
    
    def get_absolute_url(self):
        return '{}'.format(self.pk)
    
    def save(self, *args, **kwargs):
        self.passing_score = self.perfect_score*0.6
        super(Assignment, self).save(*args, **kwargs)


