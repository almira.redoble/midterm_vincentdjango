from django.urls import path
from .views import homeAssignmentsPage, AssignmentsDetailView, AssignmentsUpdateView, AssignmentsCreateView

urlpatterns = [
    path('', homeAssignmentsPage, name='homePage'),
    path('add/', AssignmentsCreateView.as_view(), name='assignment_add'),
    path('<int:pk>/details/', AssignmentsDetailView.as_view(), name='assignment_details'),
    path('<int:pk>/edit/', AssignmentsUpdateView.as_view(), name='assignment_update'),
]

app_name = "assignments"