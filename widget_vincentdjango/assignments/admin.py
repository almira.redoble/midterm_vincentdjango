from django.contrib import admin
from .models import Assignment, Course


class AssignmentInline(admin.StackedInline):
    model = Assignment


class AssignmentAdmin(admin.ModelAdmin):
    model = Assignment

    list_display = ('assignment_name', 'course',)
    search_fields = ('course',)
    list_filter = ('course', 'perfect_score', 'passing_score')


class CourseAdmin(admin.ModelAdmin):
    model = Course

    list_display = ('course_code', 'course_title','section',)
    search_fields = ('course_code', 'course_title','section',)
    list_filter = ('course_code', 'course_title','section',)


admin.site.register(Course, CourseAdmin)
admin.site.register(Assignment, AssignmentAdmin)