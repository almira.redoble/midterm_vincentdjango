# forum/models.py
from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse
from datetime import datetime

class ForumPost(models.Model):
    title = models.CharField(max_length=100)
    body = models.CharField(max_length=200)
    author = models.ForeignKey(
        WidgetUser,
        null=True,
        default=True,
        on_delete=models.CASCADE,
    )
    pub_datetime = models.DateTimeField(
        default=datetime.now(),
        editable=False,
    )

    def __str__(self):
        return self.title

    def format_pub_datetime(self):
        return self.pub_datetime.strftime('%m/%d/%Y %I:%M %p')

    def get_absolute_url(self):
        return reverse('forum:forumpost-details', kwargs={'pk': self.pk})


class Reply(models.Model):
    post = models.ForeignKey(
        ForumPost,
        null=True,
        default=True,
        on_delete=models.CASCADE,
        related_name="replies",
    )
    body = models.CharField(max_length=200)
    author = models.ForeignKey(
        WidgetUser,
        null=True,
        default=True,
        on_delete=models.CASCADE,
    )
    pub_datetime = models.DateTimeField()

    def __str__(self):
        return self.body

    def format_pub_datetime(self):
        return self.pub_datetime.strftime('%m/%d/%Y %I:%M %p')
    
