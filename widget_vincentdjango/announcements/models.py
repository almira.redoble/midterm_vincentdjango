from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse


class Announcement(models.Model):
    title = models.CharField(max_length=250, default="")
    body = models.TextField(null=True, blank=True)
    author = models.ForeignKey(
        WidgetUser,
        null=True, 
        default=True, 
        on_delete=models.CASCADE,
        related_name='announcements',
    )
    pub_datetime = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return self.title
    
    def format_pub_datetime(self):
        return self.pub_datetime.strftime('%m/%d/%Y, %I:%M %p')
    
    def get_absolute_url(self):
        return reverse('announcements:announcements-detail', kwargs={'pk': self.pk})

class Reaction(models.Model):
    name = models.CharField(max_length=5, choices=[('Like', 'Like'), 
                                                   ('Love', 'Love'), 
                                                   ('Angry', 'Angry')], default='Like')
    tally = models.PositiveIntegerField(default=0)
    announcement = models.ForeignKey(
        Announcement, 
        null=True, 
        default=True, 
        on_delete=models.CASCADE,
        related_name='reactions'
    )

    def __str__(self):
        return self.name
    
