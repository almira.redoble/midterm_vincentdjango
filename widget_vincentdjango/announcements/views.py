from django.shortcuts import render
from django.http import HttpResponse
from .models import Announcement
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from dashboard.models import WidgetUser

def index(request):
    announcements = Announcement.objects.all()
    return render(request, 'announcements/announcements.html', {'announcements':announcements})

class AnnouncementsDetailView(DetailView):
    model = Announcement
    template_name = 'announcements/announcement-details.html'

class AnnouncementsCreateView(CreateView):
    model = Announcement
    template_name = 'announcements/announcement-add.html'
    fields = ["title", "body", "author"]

class AnnouncementsUpdateView(UpdateView):
    model = Announcement
    template_name = 'announcements/announcement-edit.html'
    fields = '__all__'