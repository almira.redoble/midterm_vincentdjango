from django.contrib import admin
from .models import Department, WidgetUser


class WidgetUserInline(admin.TabularInline):
    model = WidgetUser


class WidgetUserAdmin(admin.ModelAdmin):
    model = WidgetUser

    list_display = ('last_name', 'first_name', 'department')
    search_fields = ('last_name', 'first_name', 'department')
    list_filter = ('last_name', 'department')

    fieldsets = [
        ('Name', {
            'fields':
                (('last_name', 'first_name'), 'middle_name', 'department',),
        }),
    ]


class DepartmentAdmin(admin.ModelAdmin):
    model = Department

    list_display = ('dept_name', 'home_unit')
    search_fields = ('dept_name', 'home_unit')
    list_filter = ('home_unit', )

    fieldsets = [
        ('Details', {
            'fields':
                (('dept_name', 'home_unit'),),
        }),
    ]

    inlines = [WidgetUserInline, ]


admin.site.register(Department, DepartmentAdmin)
admin.site.register(WidgetUser, WidgetUserAdmin)
