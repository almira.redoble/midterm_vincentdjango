from django import forms
from .models import WidgetUser

class UserForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-design'

    class Meta:
        model = WidgetUser
        fields = '__all__'
        labels = {
            'first_name': ('First Name'),
            'middle_name': ('Middle Name'),
            'last_name': ('Last Name'),
            'department': ('Department, Home Unit'),
        }
        